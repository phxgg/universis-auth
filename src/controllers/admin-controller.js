import { HttpServiceController } from '@themost/web';
import { httpController } from '@themost/web/decorators'
@httpController()
class AdminController extends HttpServiceController {
    
    constructor(context) {
        super(context);
    }
}

module.exports = AdminController;
