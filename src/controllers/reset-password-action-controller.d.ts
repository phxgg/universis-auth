import { HttpBaseController } from "@themost/web";
declare class ResetPasswordActionController extends HttpBaseController {

    postPasswordReminder(email: string): Promise<any>;
    getPasswordReset(code: string): Promise<any>;
    postPasswordReset(code: string, newPassword: string, confirmPassword: string): Promise<any>;
    
}
export = ResetPasswordActionController;