import { HttpForbiddenError, HttpServerError } from '@themost/common';
import { ScopeAccessConfiguration } from '../services/scope-access-configuration';
/**
 * @class
 * @augments AuthenticateRequestHandler
 */
export class ScopeAccessHandler {
    static createInstance() {
        return new ScopeAccessHandler();
    }
    authenticateRequest(context, callback) {
        if (context) {
            const scopeAccess = context.getApplication().getConfiguration().getStrategy(ScopeAccessConfiguration);
            if (scopeAccess == null) {
                return callback(new HttpServerError('Scope access cannot be validated because a required service cannot be found'));
            }
            Object.defineProperty(context.request, 'context', {
                get: function() {
                    return context;
                }
            });
            return scopeAccess.verify(context.request).then((result) => {
                if (result == null) {
                    return callback(new HttpForbiddenError('Access denied due to authorization scopes.'));
                }
                return callback();
            }).catch((err) => {
                return callback(err);
            });
        }
        return callback();
    }
}

/**
 * @returns {ScopeAccessHandler}
 */
 export function createInstance() {
    return new ScopeAccessHandler();
}