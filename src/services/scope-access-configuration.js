 import {ConfigurationStrategy, Args} from "@themost/common";
 import path from 'path';
 import {URL} from 'url';
 
 const HTTP_METHOD_REGEXP = /^\b(POST|PUT|PATCH|DELETE)\b$/i;
 
 class ScopeAccessConfiguration extends ConfigurationStrategy {
     /**
      * @param {ConfigurationBase} configuration
      */
     constructor(configuration) {
         super(configuration);
         let elements = [];
         // define property
         Object.defineProperty(this, 'elements', {
             get: () => {
                 return elements;
             },
             enumerable: true
         });
     }
 
     /**
      * @param {Request} req
      * @returns Promise<ScopeAccessConfigurationElement>
      */
     verify(req) {
         return new Promise((resolve, reject) => {
             try {
                 // validate request context
                 Args.notNull(req.context,'Context');
                 const authenticationScope = (req.context && req.context.user && req.context.user.authenticationScope) || 'anonymous';
                 if (authenticationScope && authenticationScope.length>0) {
                     // get original url
                     let reqUrl = new URL(req.originalUrl || req.url, "http://localhost").pathname;
                     const baseUrl = this.getConfiguration().getSourceAt('settings/app/base') || '/';
                     reqUrl = reqUrl.replace(new RegExp('^' + baseUrl, 'ig'), '/');
                     // get user context scopes as array e.g, ['students', 'students:read']
                     let reqScopes = authenticationScope.split(',');
                     // get user access based on HTTP method e.g. GET -> read access
                     let reqAccess = HTTP_METHOD_REGEXP.test(req.method) ? "write" : "read";
                     let result = this.elements.find(x => {
                         // filter element by access level
                         return x.access.indexOf(reqAccess)>=0
                             // resource path
                             && new RegExp( "^" + x.resource, 'i').test(reqUrl)
                             // and scopes
                             && x.scope.find(y => {
                                 // search user scopes (validate wildcard scope)
                                 return y === "*" || reqScopes.indexOf(y)>=0;
                             });
                     });
                     return resolve(result);
                 }
                 return resolve();
             }
             catch(err) {
                 return reject(err);
             }
 
         });
     }
 
 }
 
 /**
  * @class
  */
 class DefaultScopeAccessConfiguration extends ScopeAccessConfiguration {
     /**
      * @param {ConfigurationBase} configuration
      */
     constructor(configuration) {
        super(configuration);
         let defaults = [];
        // load scope access from configuration resource
         try {
             /**
              * @type {Array<ScopeAccessConfigurationElement>}
              */
             defaults = require(path.resolve(configuration.getConfigurationPath(), 'scope.access.json'));
         }
         catch(err) {
             // if an error occurred other than module not found (there are no default access policies)
             if (err.code !== 'MODULE_NOT_FOUND') {
                 // throw error
                 throw err;
             }
             // otherwise continue
         }
         this.elements.push.apply(this.elements, defaults);
     }
 }
 
 export {
    ScopeAccessConfiguration,
    DefaultScopeAccessConfiguration
 }