
import {DataObject} from '@themost/data/data-object';
import {EdmMapping} from '@themost/data/odata';
/**
 * @class
 * @property {string} client_id
 * @property {string} user_id
 * @property {Date} expires
 * @property {string} access_token
 * @augments {DataObject}
 */
@EdmMapping.entityType('AccessToken')
class AccessToken extends DataObject {
    constructor() {
        super();
        this.selector('expired', (callback)=> {
            const expired = (new Date(this.expires)).getTime()<(new Date()).getTime();
            return callback(null, expired);
        });
    }
}

module.exports = AccessToken;