import Thing = require('./thing-model');

/**
 * @class
 */
declare class Account extends Thing {

     public id: number; 
     
     public accountType?: number; 

}

export = Account;