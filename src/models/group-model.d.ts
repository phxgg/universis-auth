import {EdmMapping,EdmType} from '@themost/data';
import Account = require('./account-model');

/**
 * @class
 */
declare class Group extends Account {
     
     public id: number; 
     
     public members?: Array<Account|any>; 

}

export = Group;