import { DataObject } from "@themost/data";
declare class Action extends DataObject {
    isOverdue(): Promise<boolean>;
}
export = Action;