import {EdmMapping, DataObject} from '@themost/data';

@EdmMapping.entityType('UserAttribute')
class UserAttribute extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    async toAccessToken(token) {
        const mappings = await this.context.model('UserAttributeMappings').where('accessTokenClaim').equal(true).silent().getItems();
        if (mappings.length === 0) {
            return token;
        }
        // eslint-disable-next-line no-empty-pattern
        mappings.forEach((mapping) => {
            Object.defineProperty(token, mapping.claimName, {
                configurable: true,
                enumerable: true,
                value: this[mapping.name]
            });
        });
        return token;
    }

    async toUser(user) {
        const mappings = await this.context.model('UserAttributeMappings').where('userInfoClaim').equal(true).silent().getItems();
        if (mappings.length === 0) {
            return user;
        }
        // eslint-disable-next-line no-empty-pattern
        mappings.forEach((mapping) => {
            Object.defineProperty(user, mapping.claimName, {
                configurable: true,
                enumerable: true,
                value: this[mapping.name]
            });
        });
        return user;
    }

}
module.exports = UserAttribute;