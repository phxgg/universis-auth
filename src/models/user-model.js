import {DataObject, EdmMapping} from "@themost/data";

@EdmMapping.entityType('User')
/**
 * @class
 * @augments {DataObject}
 */
class User extends DataObject {
    constructor() {
        super();
    }
}

module.exports = User;