import {DataObject} from '@themost/data';
/**
 * @class
 */
declare class Thing extends DataObject {

     public id: number; 
     
     public additionalType?: string; 
     
     public alternateName?: string; 
     
     public description?: string; 
     
     public image?: string; 
     
     public name?: string; 
     
     public url?: string; 
     
     public dateCreated?: Date; 
     
     public dateModified?: Date; 
     
     public createdBy?: any; 
     
     public modifiedBy?: any; 

}

export = Thing;