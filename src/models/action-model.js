
import {DataObject} from '@themost/data/data-object';
import {EdmMapping} from '@themost/data/odata';
import moment from 'moment';
@EdmMapping.entityType('Action')
class Action extends DataObject {
    constructor() {
        super();
        this.selector('overdue', (callback)=> {
            if (Object.prototype.hasOwnProperty.call(this, 'endTime')) {
                const endTime = moment(this.endTime);
                if (endTime.isValid()) {
                    return callback(null, endTime.toDate()<(new Date()));
                }
            }
            return this.getModel()
                .where('id').equal(this.getId())
                .silent()
                .select('endTime').value().then((value)=> {
                    const endTime = moment(value);
                    if (endTime.isValid()) {
                        return callback(null, endTime.toDate()<(new Date()));
                    }
                    return callback(null, false);
                }).catch((err) => {
                return callback(err);
            });
        });
    }

    isOverdue() {
        return this.is(':overdue');
    }

}

module.exports = Action;