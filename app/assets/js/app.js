angular.module('app', []).controller('LoginController', function($scope, $location) {
    var vm = this;
    function activate() {
        //
    }
    activate();

}).controller('ResetPasswordController', function($scope) {

    var vm = this;
    function activate() {
        //
        vm.passwordPattern = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z\\%\\,\\;\\&\\$\\?\\^\\&\\@\\#\\!\\(\\)\\[\\]\\-\\+\\_\\=]{8,}$";
    }
    activate();

}).directive('textUppercase', function() {
    return {
        restrict: 'C',
        link: function (scope, element) {
            let text = element.html();
            if (text) {
                element.html(text.replace(/[ά]/ig, 'α')
                    .replace(/[έ]/ig, 'ε')
                    .replace(/[ή]/ig, 'η')
                    .replace(/[ίΐϊ]/g, 'ι')
                    .replace(/[ό]/ig, 'ο')
                    .replace(/[ύΰϋ]/ig, 'υ')
                    .replace(/[ώ]/ig, 'ω'));
            }
        }
    };
}).directive("compareTo", function() {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {
            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue === scope.otherModelValue;
            };
            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
});